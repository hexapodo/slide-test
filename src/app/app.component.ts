import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  imageUrlArray = [
    'https://1.img-dpreview.com/files/p/TS1200x900~sample_galleries/7680830787/1713634509.jpg',
    'https://1.img-dpreview.com/files/p/TS1200x900~sample_galleries/7680830787/5983202710.jpg',
    'https://1.img-dpreview.com/files/p/TS1200x900~sample_galleries/0753964569/0238604422.jpg',
    'https://4.img-dpreview.com/files/p/TS600x450~sample_galleries/7780769194/7454011089.jpg',
    'https://3.img-dpreview.com/files/p/TS1200x900~sample_galleries/1434441347/7297434329.jpg'
  ];
}
